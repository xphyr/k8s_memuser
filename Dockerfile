FROM golang:alpine as builder
RUN mkdir /build 
ADD . /build/
WORKDIR /build/src 
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o memuser .
FROM alpine
COPY --from=builder /build/src/memuser /app/
WORKDIR /app
EXPOSE 8080
CMD ["/app/memuser", "-fast", "-maxmemory", "1000"]