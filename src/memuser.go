package main

import (
	"crypto/rand"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"runtime/debug"
	"syscall"
	"time"
)

var (
	overall [][]byte
	memPtr  *int
	fastPtr *bool
	maxMem  *int
)

func main() {

	memPtr = flag.Int("memory", 50, "how much memory to consume")
	maxMem = flag.Int("maxmemory", 1000, "dont consume more than this")
	fastPtr = flag.Bool("fast", true, "build up memory usage quickly")

	flag.Parse()

	// enable signal trapping
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c,
			syscall.SIGINT,  // Ctrl+C
			syscall.SIGTERM, // Termination Request
			syscall.SIGSEGV, // FullDerp
			syscall.SIGABRT, // Abnormal termination
			syscall.SIGILL,  // illegal instruction
			syscall.SIGFPE)  // floating point
		sig := <-c
		fmt.Println("-----------------------------------------")
		fmt.Printf("Signal (%v) Detected, Shutting Down\n", sig)
		fmt.Println("Final Memory usage when killed:")
		fmt.Printf(ReturnMemUsage())
		os.Exit(1)
	}()

	http.HandleFunc("/", HelloServer)
	http.HandleFunc("/consumemem", ConsumeMemory)
	http.HandleFunc("/clearmem", ClearMemory)
	http.ListenAndServe(":8080", nil)

}

// ReturnMemUsage returns a string showing the current memory usage.
func ReturnMemUsage() string {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)

	return fmt.Sprintf("Alloc = %v MiB\tSys = %v MiB\tNumGC = %v\n", bToMb(m.Alloc), bToMb(m.Sys), m.NumGC)

}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}

// AllocateMemory will allocate memory on the shared byte array
func AllocateMemory() {

	var m runtime.MemStats
	runtime.ReadMemStats(&m)

	if bToMb(m.Alloc) <= uint64(*maxMem) {
		for i := 0; i < *memPtr; i++ {

			// Allocate memory using make() and append to overall (so it doesn't get
			// garbage collected). This is to create an ever increasing memory usage
			// which we can track. We're just using []int as an example.
			a := make([]byte, 1048576)
			rand.Read(a)
			//for j := 0; j < 1024; j++ {
			overall = append(overall, a)
			//}

			if !*fastPtr {
				time.Sleep(time.Second)
			}
		}
	}
	fmt.Printf(ReturnMemUsage())
}

// HelloServer will return a hello world, and will consume some more memory in the process
func HelloServer(w http.ResponseWriter, r *http.Request) {
	message := fmt.Sprintf("Hello User. My current memory usage is:\n %v", ReturnMemUsage())

	fmt.Fprintf(w, message)

}

// ConsumeMemory will return a hello world, and will consume some more memory in the process
func ConsumeMemory(w http.ResponseWriter, r *http.Request) {
	AllocateMemory()
	message := fmt.Sprintf("Hello User. My current memory usage is:\n %v", ReturnMemUsage())

	fmt.Fprintf(w, message)

}

// ClearMemory will return a hello world, and will consume some more memory in the process
func ClearMemory(w http.ResponseWriter, r *http.Request) {
	overall = nil
	runtime.GC()
	debug.FreeOSMemory()
	message := fmt.Sprintf("Memory has been cleared.\n %v", ReturnMemUsage())
	fmt.Fprintf(w, message)

}
